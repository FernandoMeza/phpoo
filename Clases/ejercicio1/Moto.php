<!--
	Nombre: Meza Ortega Fernando
	Ejercicio 1 PHPOO
-->	

<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto
{
	public $tipo;
	public $marca;
}

//Mensaje del servidor que en un inicio será nulo hasta que reciba lo de post
$mensajeServidorMoto = '';

//crea aqui la instancia o el objeto de la clase Moto
$moto1 = new Moto;

if ( !empty($_POST))
{
	
	// recibe aqui los valores mandados por post y arma el mensaje para front
	$moto1->tipo = $_POST['tipo'];
	$moto1->marca = $_POST['marca'];

	//Ahora construimos el mensaje
	$mensajeServidorMoto = "El servidor dice que ya elegiste una moto de tipo ";
	$mensajeServidorMoto .= $_POST['tipo'] . " y de marca " . $_POST['marca'];
	$mensajeServidorMoto .= ".";

}  


?>
