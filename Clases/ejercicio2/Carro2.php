<!--
	Nombre: Meza Ortega Fernando
	Ejercicio 2
-->

<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $verificado;

	//declaracion del método verificación, es estático porque es un método que
	//no depende de los atributos del objeto
	public static function verificacion($anio){
		if (!isset($anio) || empty($anio))
			return "";
		$num = intval($anio);
		if ($num < 1990)
			return "No";
		else if ($num <= 2010)
			return "Revisión";
		else
			return "Si";

	}

	//El clásico getter y setter, para realizar verificaciones adicionales
	public function get_verificado()
	{
		return $this->verificado;
	}

	public function set_verificado($cadena){
		$this->verificado = $cadena;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

//Variable de verificación del carro

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	//Una alternativa un poco más simple es que el método verificación fuera un
	//método normal y no de clase para que modificara directamente el atribudo
	//de $verificado
	$Carro1->set_verificado(Carro2::verificacion(substr($_POST['anio'], 0, 4)));
}




