<?php  
include_once('carro.php');
include_once('avion.php');
include_once('barco.php');
include_once('tren.php');

$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre/carro':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;
		case 'terrestre/tren':
			//Tome como modelo un tren bala de Japón
			$tren1= new Tren('Shinkansen','600','Electricidad','Bala');
			$mensaje=$tren1->resumenTren();
			break;	
	}

}

?>
